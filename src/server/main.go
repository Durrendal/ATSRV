package main

import (
	//"os"
	"fmt"
	"log"
	//"syscall"
	"net/http"
	"encoding/json"

	//"gopkg.in/yaml.v2"
	"github.com/prometheus/procfs"
)

const (
	B = 1
	KB = 1024 * B
	MB = 1024 * KB
	GB = 1024 * MB
)

type hwinfo struct {
	CPU string `json:"CPU"`
	Cores uint `json:"Cores"`
	RAM float64 `json:"RAM"`
	SWAP float64 `json:"SWAP"`
}

type netperf struct {
	TX uint64 `json:"TX"` 
	RX uint64 `json:"RX"`
	TXD uint64 `json:"TXD"`
	RXD uint64 `json:"RXD"`
	TXE uint64 `json:"TXE"`
	RXE uint64 `json:"RXE"`
}

type fwperf struct {
	Processed uint32 `json:"Processed"`
	Dropped uint32 `json:"Dropped"`
}

type discinfo struct {
	Total uint64 `json:"All"`
	Used uint64 `json:"Used"`
	Free uint64 `json:"Free"`
}

type sysload struct {
	One float64 `json:"One"`
	Five float64 `json:"Five"`
	Fifteen float64 `json:"Fifteen"`
	Free float64 `json:"Free"`
	Sfree float64 `json:"Sfree"`
	//P10 float64 `json:"P10"`
	//P60 float64 `json:"P60"`
	//P300 float64 `json:"P300"`
	//Pressure float64 `json:"Pressure"`
}

func get_hwinfo() *hwinfo {
	fs, _ := procfs.NewFS("/proc/")

	minf, err := fs.Meminfo()
	if err != nil {
		log.Fatalf("Could not get process stat: %s", err)
	}

	cinf, err := fs.CPUInfo()
	if err != nil {
		log.Fatalf("Could not get process stat: %s", err)
	}
	
	//Only inclusive of single core, may be inaccurate.
	return &hwinfo{
		CPU: cinf[0].ModelName,
		Cores: cinf[0].CPUCores,
		RAM: float64(*minf.MemTotal) / float64(KB),
		SWAP: float64(*minf.SwapTotal) /float64(KB),
	}
}

func get_netperf(inf string) *netperf {
	fs, _ := procfs.NewFS("/proc/")
	
	ninf, err := fs.NetDev()
	if err != nil {
		log.Fatalf("Could not get process stat: %s", err)
	}

	return &netperf{
		TX: ninf[inf].TxPackets,
		RX: ninf[inf].RxPackets,
		TXD: ninf[inf].TxDropped,
		RXD: ninf[inf].RxDropped,
		TXE: ninf[inf].TxErrors,
		RXE: ninf[inf].RxErrors,
	}
}

func get_fwperf() *fwperf {
	fs, _ := procfs.NewFS("/proc/")
	
	nfinf, err := fs.NetSoftnetStat()
	if err != nil {
		log.Fatalf("Could not get process stat: %s", err)
	}

	cinf, err := fs.CPUInfo()
	if err != nil {
		log.Fatalf("Could not get process stat: %s", err)
	}

	Cores := int(cinf[0].CPUCores)
	Total_Processed := uint32(0)
	Total_Dropped := uint32(0)

	//Return total processed & dropped packets by all cpu cores
	for i := 0; i < Cores; i++ {
		Total_Processed += nfinf[i].Processed
		Total_Dropped += nfinf[i].Dropped
	}

	return &fwperf{
		Processed: Total_Processed,
		Dropped: Total_Dropped,
	}
}

func get_sysload() *sysload {
	fs, _ := procfs.NewFS("/proc/")

	linf, err := fs.LoadAvg()
	if err != nil {
		log.Fatalf("Could not get process stat: %s", err)
	}

	minf, err := fs.Meminfo()
	if err != nil {
		log.Fatalf("Could not get process stat: %s", err)
	}

	//pinf, err := fs.PSILine()
	//if err != nil {
	//	log.Fatalf("Could not get process stat: %s", err)
	//}

	return &sysload{
		One: linf.Load1,
		Five: linf.Load5,
		Fifteen: linf.Load15,
		Free: float64(*minf.MemAvailable) / float64(KB),
		Sfree: float64(*minf.SwapFree) / float64(KB),
		//P10: pinf.Avg10,
		//P60: pinf.Avg60,
		//P300: pinf.Avg300,
		//Pressure: pinf.Total,
	}
}

func ATSRV(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("ATSRV Queried: %s\n", r.URL.Path)

	if r.URL.Path == "/hwinfo" {
		atsrv := get_hwinfo()
		json.NewEncoder(w).Encode(atsrv)
	} else if r.URL.Path == "/netperf" {
		inf, ok := r.URL.Query()["inf"]
		fmt.Printf("ATSRV key: %s\n", inf[0])
		
		if !ok || len(inf[0]) < 1 {
			log.Println("Interface name not specified!")
			return
		}
		atsrv := get_netperf(string(inf[0]))
		json.NewEncoder(w).Encode(atsrv)
	} else if r.URL.Path == "/fwperf" {
		atsrv := get_fwperf()
		json.NewEncoder(w).Encode(atsrv)
	} else if r.URL.Path == "/sysload" {
		atsrv := get_sysload()
		json.NewEncoder(w).Encode(atsrv)
	}
}

func handleReq() {
	http.HandleFunc("/", ATSRV)
	log.Fatal(http.ListenAndServe(":8091", nil))
}

func main() {
	handleReq()
}


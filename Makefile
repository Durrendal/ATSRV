DESTDIR ?= /usr/local/bin

compile-server:
	cd src/server && go build

install-server:
	install -Dm 755 ./src/server/atsrv $(DESTDIR)/atsrv

install-openrc:
	install -Dm 755 ./init/openrc/atsrv /etc/init.d/

# What?

Atentu Server, a simple REST server to expose system performance information.

# Why?

You should use a real monitoring system for monitoring and alerting, ATSRV was written to integrate with so that a single IRC bot could monitor, report, and be queried on system performance information.

# Building:

```
make compile-server
```

or

```
cd src/server && go build
```

# ATSRV Endpoints:

ATSRV exposes the following information via the following URI endpoints.

```
/hwinfo
  - Basic Hardware info, such as CPU type, available RAM and Swap

/sysload
  - Load averages, available RAM and SWAP

/fwperf
 - Packets processed or filtered by iptables

/netperf?inf=X
 - TX/RX statistics for a specified interface 
```

Currently netperf is the only endpoint that requires an argument, you may query it like so:

```
curl http://127.0.0.1:8091/netperf?inf=wlan0
```

Here's a quick example of the information ATSRV returns from each URI, the below example was running on a Motorola Droid4 running Alpine Linux with hand rolled kernel, some information is missing because of the odd hardware & some bad kernel configuration.

```
~|>> curl http://127.0.0.1:8091/hwinfo
{"CPU":"ARMv7 Processor rev 3 (v7l)","Cores":0,"RAM":1019340,"SWAP":253948}

~|>> curl http://127.0.0.1:8091/sysload
{"One":0.78,"Five":0.77,"Fifteen":0.79,"Free":524284,"Sfree":253948}

~|>> curl http://127.0.0.1:8091/fwperf
{"Processed":37551,"Dropped":0}

~|>> curl http://127.0.0.1:8091/netperf?inf=wlan0
{"TX":105968,"RX":87709,"TXD":0,"RXD":5,"TXE":0,"RXE":0}
```
